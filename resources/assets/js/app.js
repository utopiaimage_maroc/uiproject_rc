
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

jQuery(document).ready(function(){
     jQuery('#datetimepicker_day').datetimepicker({
       format:"YYYY/MM/DD",
       useCurrent: false,
   		 minDate:jQuery('#datetimepicker_day').attr("min"),
       maxDate:jQuery('#datetimepicker_day').attr("max")
     });

     jQuery('#datetimepicker_day').on("dp.change", function (e) {
         var datex = moment(e.date).format('YYYY-MM-DD');
         jQuery('#date_hidden').val(datex);
         jQuery('#form_day').submit();
     });

     jQuery('#datetimepicker_end').datetimepicker({
       format:"YYYY/MM/DD"
     });

     jQuery('#datetimepicker_start').datetimepicker({
       format:"YYYY/MM/DD"
     });
 });
