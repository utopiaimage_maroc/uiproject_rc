@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="container">
                    <div class="row">
                      <div class='col-md-2'>
                          Date
                          <form method="get" id="form_day" >
                            <input type="hidden" name="date" id="date_hidden" >
                          </form>
                          <input min="{{$date_last}}" max="{{$date_now}}" type="text" class="form-control" value="{{$date}}" id="datetimepicker_day" />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-body">
                  <h4>Work in progress</h4>
                    <form class="form-horizontal" id="form_project" role="form" method="POST" action="{{ url('/store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="date_worked" value="{{ $date }}">
                    @foreach($projects as $project)
                    <div class="thumbnail">
                      <h1>{{ $project->name }}</h1>
                        @if(array_key_exists($project->id, $workings))
                          <input type="number" min="0" max="100" name="percent[{{ $project->id }}]" id="percent" value="{{$workings[$project->id]['percent']}}" placeholder="0">
                        @else
                          <input type="number" min="0" max="100" name="percent[{{ $project->id }}]" id="percent" placeholder="0">
                        @endif
                        <label for="percent" >%</label>
                    </div>
                    @endforeach
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a class="btn btn-default" href="{{url('/home')}}">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
