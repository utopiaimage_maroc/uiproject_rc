@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
          <div class="panel-heading">
            <a class="btn btn-primary pull-right" href="{{url('/project/create')}}">Add Project</a>
            <h1>Projects</h1>
          </div>
          <div class="panel-body">
          <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td class="col-md-2">ID</td>
                    <td class="col-md-3">Name</td>
                    <td class="col-md-3">Start Date</td>
                    <td class="col-md-3">End Date</td>
                    <td class="col-md-1"></td>
                </tr>
            </thead>
            <tbody>
            @foreach($projects as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <?php
                      $start_date = new \DateTime($value->start_date);
                      $start_date = $start_date->format("Y/m/d");
                    ?>
                    <td>{{ $start_date }}</td>
                    <?php
                      $end_date = new \DateTime($value->end_date);
                      $end_date = $end_date->format("Y/m/d");
                    ?>
                    <td>{{ $end_date }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>
                        <!--<a class="btn btn-small btn-success" href="{{ URL::to('project/' . $value->id) }}">Show this Project</a>-->
                        <a class="btn btn-small btn-info" href="{{ URL::to('project/' . $value->id . '/edit') }}">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <div class="panel-footer">{{ $projects->render() }}</div>
        </div>
      </div>
    </div>
</div>
@endsection
