@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <a class="btn btn-default pull-right" href="{{url('/project')}}">Back</a>
                  <h1>{{ $project->name }}</h1>
                </div>
                <div class="panel-body">
                  <strong>Start Date :</strong> {{ $project->start_date }}<br/>
                  <strong>End Date :</strong> {{ $project->end_date }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
