@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Work in Progress</h1></div>
                <div class="panel-body">
                  <table class="table">
                    <tr>
                      <th class="col-md-5"></th>
                      @foreach ( $dates as $key => $value )
                        <th class="col-md-1">
                          <div class="btn-group" role="group" aria-label="...">
                            <a href="{{ URL::to('day?date=' .$key) }}" class="btn btn-success" ><span>{{$value}}</span></a>
                          </div>
                        </th>
                      @endforeach
                    </tr>
                  @foreach ( $projects as $project )
                    <tr>
                      <td>{{$project->name}}</td>
                      @foreach ( $dates as $key => $value )
                      <td class="text-center">
                        @if(array_key_exists($key, $workin))
                          @if(array_key_exists($project->id, $workin[$key]))
                            {{$workin[$key][$project->id]}} %
                          @else
                           -
                          @endif
                        @else
                          -
                        @endif

                      </td>
                      @endforeach
                    </tr>
                  @endforeach
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
