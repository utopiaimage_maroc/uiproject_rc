@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
          <div class="panel-heading">
            <form method="get" class="form-horizontal pull-right col-md-4" >
              <div class="form-group">
              <label class="col-md-2 control-label" for="member">User:</label>
              <div class="col-md-10">
                <select class="form-control" name="member" id="member" onChange="submit()">
                  <option value="">All Users</option>
                  @foreach($users as $key => $value)
                  <option value="{{$value->id}}" @if($member == $value->id) selected="selected" @endif>{{$value->name}}</option>
                  @endforeach
                </select>
              </div>
              </div>
            </form>
            <h1>Progress Report</h1>
          </div>
          <div class="panel-body">
          <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td class="col-md-1">ID</td>
                    <td class="col-md-4">Project</td>
                    <td class="col-md-4">Member</td>
                    <td class="col-md-1">Percent</td>
                    <td class="col-md-2">Date modified</td>
                </tr>
            </thead>
            <tbody>
            @foreach($workings as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->project->name }}</td>
                    <td>{{ $value->user->name }}</td>
                    <td>{{ $value->percent }} %</td>
                    <?php
                      $date_worked = new \DateTime($value->date_worked);
                      $date_worked = $date_worked->format("Y/m/d");
                    ?>
                    <td>{{ $date_worked }}</td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <div class="panel-footer">@if($member) {{ $workings->appends(['member' => $member])->render() }} @else {{ $workings->render() }} @endif</div>
        </div>
      </div>
    </div>
</div>
@endsection
