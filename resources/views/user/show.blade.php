@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a class="btn btn-default pull-right" href="{{url('/user')}}">Back</a><h1>{{ $user->name }}</h1></div>

                <div class="panel-body">
                  <strong>ID :</strong> {{ $user->id }}<br/>
                  <strong>E-mail :</strong> {{ $user->email }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
