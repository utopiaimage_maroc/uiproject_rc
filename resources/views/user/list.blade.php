@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
          <div class="panel-heading">
            <a class="btn btn-primary pull-right" href="{{url('/user/create')}}">Add User</a>
            <h1>Users</h1>
          </div>
          <div class="panel-body">
          <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td class="col-md-1">ID</td>
                    <td class="col-md-5">E-mail</td>
                    <td class="col-md-4">Name</td>
                    <td class="text-center" class="col-md-1">Admin</td>
                    <td class="col-md-1"></td>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->name }}</td>
                    <td class="text-center"><input type="checkbox" @if($value->admin) checked="checked"  @endif disabled="disabled"></td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>
                        <!--<a class="btn btn-small btn-success" href="{{ URL::to('user/' . $value->id) }}">Show this user</a>-->
                        <a class="btn btn-small btn-info" href="{{ URL::to('user/' . $value->id . '/edit') }}">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <div class="panel-footer">{{ $users->render() }}</div>
        </div>
      </div>
    </div>
</div>
@endsection
