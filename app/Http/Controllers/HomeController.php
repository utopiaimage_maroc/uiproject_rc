<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Working;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $date_now = date('Y-m-d');
      $date_last = date('Y-m-d', strtotime('-6 days'));
      $start = new \DateTime($date_last);
      $end = new \DateTime($date_now);
      $end = $end->modify( '+1 day' );
      $interval = \DateInterval::createFromDateString('1 day');
      $period = new \DatePeriod($start, $interval, $end);
      $projects = Project::where('start_date', '<=', $date_now)->where('end_date', '>=', $date_last)->get();
      $workin = array();
      $dates = array();

      foreach ( $period as $dt ){
        $date = $dt->format( "Y-m-d" );
        $dates[$date] = $dt->format("Y/m/d");
        $workings = Working::where('date_worked', $date)->where('users_id', \Auth::user()->id)->get();
        foreach ( $workings as $working ){
          $workin[$date][$working->projects_id] = $working->percent;
        }
      }
      $dates = array_reverse($dates);
      return view('home')->with('projects', $projects)->with('workin', $workin)->with('dates', $dates);
    }

    public function day(Request $request)
    {
      $date_now = date('Y-m-d');
      $date_last = date('Y-m-d', strtotime('-6 days'));
      if($request->date){
        $date = $request->date;
        if($date < $date_last OR $date > $date_now){
          return redirect('home')->with('message','Please select valid date !');
        }
      }else{
        $date = $date_now;
      }
      $projects = Project::where('start_date', '<=', $date)->where('end_date', '>=', $date)->get();
      if(!count($projects)){
          return redirect('home')->with('message','No project exists on this date');
      }
      $workings = Working::where('date_worked', $date)->where('users_id', \Auth::user()->id)->get();
      $workin = array();
      foreach ($workings as $working) {
        $workin[$working->projects_id] = ["id" => $working->id, "percent" => $working->percent];
      }
      return view('day')->with('date', $date)->with('projects', $projects)->with('workings', $workin)->with('date_now', $date_now)->with('date_last', $date_last);
    }

    public function store(Request $request)
    {
        //
        $total = 0;
        $date_worked = $request->date_worked;
        foreach ($request->percent as $project_id => $percent) {
          $total += $percent;
        }

        if($total == 100){
          foreach ($request->percent as $project_id => $percent) {
            $working_one = Working::where('date_worked', $date_worked)->where('projects_id', $project_id)->first();
            if($percent){
              if($working_one){
                $working = Working::find($working_one->id);
              }else{
                $working = new Working;
              }
              $working->users_id = \Auth::user()->id;
              $working->projects_id = $project_id;
              $working->percent = $percent;
              $working->date_worked = $date_worked;
              $working->save();
            }
          }
          return redirect('home')->with('message','Progress day has been Edited !');
        }else{
          return redirect('day?date='.$date_worked)->with('message','The total percentage is not 100%, please change your percentage for each project.');
        }
    }
}
