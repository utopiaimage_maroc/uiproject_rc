<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Working;
use App\User;

class WorkingController extends Controller {

    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //
        $users = User::all();
        $member = $request->member;
        if($request->member){
          $workings = Working::where('users_id', $request->member)->orderBy('date_worked', 'desc')->paginate(20);
        }else{
          $workings = Working::orderBy('date_worked', 'desc')->paginate(20);
        }
        return view('working/list')->with('workings', $workings)->with('users', $users)->with('member', $member);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $working = new Working;
        $working->users_id = \Auth::user()->id;
        $working->projects_id = $request->project_id;
        $working->percent = $request->percent;
        $working->date_worked = $request->date_worked;
        $msg = 'Progress <strong>'.$working->date_worked.'</strong> has been created !';
        $working->save();

        return redirect('day?date='.$request->date_worked)->with('message',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        //
        $working = Working::find($id);
        $working->users_id = \Auth::user()->id;
        $working->projects_id = $request->project_id;
        $working->percent = $request->percent;
        $working->date_worked = $request->date_worked;
        $msg = 'Progress <strong>'.$working->date_worked.'</strong> has been Edited !';
        $working->save();

        return redirect('day?date='.$request->date_worked)->with('message',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
