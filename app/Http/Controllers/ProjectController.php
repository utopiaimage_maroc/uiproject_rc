<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller {

    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the projects
        $projects = Project::paginate(20);
        return view('project/list')->with('projects', $projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('project/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $start_date = new \DateTime($request->start_date);
        $start_date = $start_date->format("Y-m-d");

        $end_date = new \DateTime($request->end_date);
        $end_date = $end_date->format("Y-m-d");

        $project = new Project;
        $project->name = $request->name;
        $project->start_date = $start_date;
        $project->end_date = $end_date;
        $msg = 'project <strong>'.$project->name.'</strong> has been created !';
        $project->save();

        return redirect('project')->with('message',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $project = Project::find($id);
        if(!$project){
          abort(404);
        }
        return view('project/show')->with('project',$project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $project = Project::find($id);
        if(!$project){
          abort(404);
        }
        return view('project/edit')->with('project',$project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        $start_date = new \DateTime($request->start_date);
        $start_date = $start_date->format("Y-m-d");

        $end_date = new \DateTime($request->end_date);
        $end_date = $end_date->format("Y-m-d");

        $project = Project::find($id);
        $project->name = $request->name;
        $project->start_date = $start_date;
        $project->end_date = $end_date;
        $msg = 'Project <strong>'.$project->name .'</strong> has been edited !';
        $project->save();

        return redirect('project')->with('message',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $project = Project::find($id);
        $msg = 'Project <strong>'.$project->name.'</strong> has been Deleted !';
        $project->delete();
        return redirect('project')->with('message',$msg);
    }

}
