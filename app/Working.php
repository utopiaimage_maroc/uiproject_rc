<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Working extends Model
{
    //

    public function user()
    {
        return $this->belongsTo('App\User','users_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project','projects_id');
    }
}
